#include <EEPROM.h>

#define EEPROM_SIZE 10

#define DEVICE_ID       "4"
#define DEVICE_SECRET   "123456"

/* ======================================== */
/* WIFI */
/* ======================================== */
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

const char WIFI_SSID[] = "AHT LAB";
const char WIFI_PASS[] = "0941732379";

WiFiClient client;
IPAddress dns(8,8,8,8);

/* ======================================== */
/* SIM800 */
/* ======================================== */
#include <aht_sim800.h>
#define MODEM_RX 16
#define MODEM_TX 17
#define uart Serial2

AHT_GSM *gsmMaster = new AHT_GSM(&uart);
AHT_GSM *gsm;
GSM_TYPE gsmType;
bool gsmOk = false;

/* ======================================== */
/* PZEM */
/* ======================================== */
#include "Hshopvn_Pzem004t_V2.h"

// RX 13 TX 15
HardwareSerial pzemSerial(1);
Hshopvn_Pzem004t_V2 pzem(&pzemSerial);
pzem_info pzemData;

#define BALANCE_ADDRESS 1
float balance = 0;
float lastE = 0;

// RELAY
#define RELAY_PIN       5
#define RELAY_ON        1
#define RELAY_OFF       0

// BUZZER
#define BUZZER_PIN       12
#define BUZZER_ON        1
#define BUZZER_OFF       0

// INET
#define APN             "internet.wind"
#define USER            ""
#define PASS            ""

// SERVER
#define HTTP            "http://"
#define HOST            "luanvan.site"

#define PATH_BALANCE    "/api/"DEVICE_ID"/"DEVICE_SECRET"/check-balance"

#define PATH_RELAY	    "/api/"DEVICE_ID"/"DEVICE_SECRET"/relay-status"

#define PATTERN_PUBLISH "/api/"DEVICE_ID"/"DEVICE_SECRET"?u=%s&i=%s&p=%s&e=%s"
char PATH_PUBLISH[128];

#define BODY_LEN        128
char body[BODY_LEN];

// TIMEOUT
#define SEND_TIMEOUT    60000L
unsigned long lastTimeSend = 0;

// Flags
bool gsmNet = false;

/* ======================================== */
/* OLED */
/* ======================================== */
#include <Wire.h> 
#include "SSD1306Wire.h"

#define DISPLAY_WIDTH 128
#define DISPLAY_HEIGHT 64

SSD1306Wire display(0x3c, SDA, SCL);

void setup() 
{
    Serial.begin(115200);
    delay(1000);

    EEPROM.begin(EEPROM_SIZE);

    display.init();
    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);

    display.clear();
    display.drawString(16, 0, "SETUP WIFI");
    display.drawString(16, 20, "AHTLAB");
    display.drawString(16, 40, "0941732379");
    display.display();

    WiFi_Init();

    display.clear();
    display.drawString(16, 20, "SETUP GSM");
    display.display();

    if(gsmMaster->begin()) 
    {
        gsmType = gsmMaster->detectGSM(&uart);
        unsigned long baudrate = gsmMaster->getBaudrate();
        free(gsmMaster);
        
        if(gsmType == SIM800) 
        {
            gsm = new AHT_SIM800(&uart);
            gsm->begin(baudrate);
            gsmOk = true;
            Serial.println("OK");
        }
        gsmNet = gsm->attackGPRS("internet.wind", "", "");
    }

    display.clear();
    display.drawString(16, 20, "LOAD DATA");
    display.display();

    pzemSerial.begin(9600, SERIAL_8N1, 15, 13);
    delay(1000);
    
    pinMode(RELAY_PIN, OUTPUT);
    pinMode(BUZZER_PIN, OUTPUT);

    readBalance();
    if (balance <= 0)
    {
        getBalanceServer();   
    }
    readPzemData();
}

void loop() 
{
    if (!gsmOk)
    {
        Serial.println("can't detect gsm module");
        delay(5000);
        ESP.restart();
        
        return;
    }

    readPzemData();
    checkBalance();

    if (millis() - lastTimeSend > SEND_TIMEOUT)
    {
        requestPublish();
        lastTimeSend = millis();
    }
    
    delay(2000);
}

void Bipbip()
{
    for (int i = 0; i < 10; i++)
    {
        PIN_Control(BUZZER_PIN, BUZZER_ON); 
        delay(200);   
        PIN_Control(BUZZER_PIN, BUZZER_OFF); 
        delay(200);   
    }
}

void readBalance()
{
    balance = EEPROM_Read(BALANCE_ADDRESS)/1000;
    delay(10);
    Serial.println("EEPROM BALANCE: " + String(balance));
}

void saveBalance()
{
    EEPROM_Write(BALANCE_ADDRESS, (int)(balance*1000));
    delay(10);
}

bool checkBalance()
{
    Serial.println("LOCAL BALANCE: " + String(balance));
    
    if (balance == 0)
    {
        Bipbip();
        Relay_Off();
        
        getBalanceServer();
        
        if (balance == 0) 
        {
            return false;
        }
        else
        {
            Relay_On();
            return true;   
        }
    }

    if (balance <= 10)
    {
        Bipbip();
        getBalanceServer();
    }
    
    //Relay_On();
	getRelay();

    return true;
}

void getBalanceServer()
{
    bool requestStatus = getRequest(HOST, PATH_BALANCE);
    if(requestStatus)
    {
        Serial.println("Request thanh cong");
        Serial.print("body: ");
        Serial.println(body);

        const char* p = strstr((const char*)body, "1:");
        if (p != nullptr)
        {
            balance = atof(p + 2);
            Serial.print("balance: ");
            Serial.println(balance);

            saveBalance();
        }
    }
    Serial.println();
}

void getRelay()
{
    bool requestStatus = getRequest(HOST, PATH_RELAY);
    if(requestStatus)
    {
        Serial.print("RELAY STATUS: ");
        Serial.println(body);

        if (strstr((const char*)body, "r1") != nullptr)
        {
            Relay_On();
        }
		
        if (strstr((const char*)body, "r0") != nullptr)
        {
            Relay_Off();
        }
    }
    Serial.println();
}

void readPzemData()
{
    pzemData = pzem.getData();
    
//    pzemData.volt = random(210, 220) + (float)random(0, 10)/10;
//    pzemData.ampe = random(5, 6) + (float)random(0, 10)/10;
//    pzemData.power = pzemData.volt * pzemData.ampe;
//    pzemData.energy = 2;

    Serial.print(pzemData.volt);
    Serial.print(F("V - "));
    Serial.print(pzemData.ampe);
    Serial.print(F("A - "));
    Serial.print(pzemData.power);
    Serial.print(F("W - "));
    Serial.print(pzemData.energy);
    Serial.print(F("Wh - "));
    Serial.print(pzemData.freq);
    Serial.print(F("Hz - "));
    Serial.print(pzemData.powerFactor);
    Serial.println();

    // Cap nhat E, tru balance
    if (lastE == 0)
    {
        lastE = pzemData.energy;
    }
    else if (lastE < pzemData.energy)
    {
        if ((pzemData.energy - lastE)/1000 > balance)
        {
            balance = 0;
        }
        else
        {
            balance -= (pzemData.energy - lastE)/1000;   
        }

        saveBalance();
        lastE = pzemData.energy;
    }

    displayOled();
}

void displayOled()
{
    display.clear();
    
    display.drawString(0, 0, "U:");
    display.drawString(16, 0, String((int)pzemData.volt));

    display.drawString(60, 0, "I:");
    display.drawString(76, 0, String(pzemData.ampe));

    display.drawString(0, 20, "P:");
    display.drawString(16, 20, String(pzemData.power));

    display.drawString(70, 20, "E:");
    display.drawString(86, 20, String(pzemData.energy/1000));

    display.drawString(0, 40, "DU: ");
    display.drawString(30, 40, String(balance) + " KWH");
    
    display.display();
}

void processResponse()
{
    if (strlen(body) == 0) return;

    if (strstr((const char *)body, "0:cost") != nullptr) 
    {
        balance = 0;
        Serial.print("Balance: ");
        Serial.println(0);

        saveBalance();
    }
    else if (strstr((const char *)body, "1:") != nullptr)
    {
        const char* p = strstr((const char*)body, "1:");
        if (p != nullptr)
        {
            balance = atof(p + 2);
            Serial.print("balance: ");
            Serial.println(balance);

            saveBalance();
        }
    }
    else
    {
        getBalanceServer();
    }
}

void requestPublish()
{
    sprintf(PATH_PUBLISH, PATTERN_PUBLISH, 
        String((int)pzemData.volt).c_str(), 
        String(pzemData.ampe).c_str(),
        String(pzemData.power).c_str(), 
        String(pzemData.energy/1000).c_str());
        
    Serial.println(PATH_PUBLISH);

    bool requestStatus = getRequest(HOST, PATH_PUBLISH);
    if(requestStatus)
    {
        Serial.println("Request thanh cong");
        Serial.print("body: ");
        Serial.println(body);

        processResponse();
    }
    Serial.println();
}

bool getRequest(const char* host, const char* path)
{
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial.println("WL_NOT_CONNECTED: GPRS Request");
        return GSM_GetRequest(host, path);
    }
    else
    {
        Serial.println("WL_CONNECTED: WIFI Request");
        return WiFi_GetRequest(host, path);
    }
}

void Relay_Off()
{
    PIN_Control(RELAY_PIN, RELAY_OFF);
}

void Relay_On()
{
    PIN_Control(RELAY_PIN, RELAY_ON);
}


void PIN_Control(uint8_t pin, uint8_t val)
{
    digitalWrite(pin, val);
}

bool GSM_GetRequest(const char* host, const char* path)
{
    if(!gsmNet)
    {
        gsmNet = gsm->attackGPRS("internet.wind", "", "");
        if(!gsmNet) return false;
    }
    
    return gsm->requestGet(host, path, 80, body, BODY_LEN);
}

bool WiFi_GetRequest(const char* host, const char* path) 
{
    HTTPClient http;
    http.begin(String(HTTP) + String(host) + String(path));    
    delay(100);
    int httpResponseCode = http.GET();
    delay(1000);
    String payload = http.getString();
    Serial.print("response: ");
    Serial.println(payload);
    
    Serial.print("Http return code: ");
    Serial.println(httpResponseCode);  

    if(httpResponseCode == 200) 
    {
        Serial.println(F("wifi request success"));
        payload.toCharArray(body, BODY_LEN);
        Serial.println(F("Disconnect server"));
        
        http.end(); 
        return true;
    }
    else
    {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpResponseCode).c_str());
    }
    
    http.end();
    return false;
}

void WiFi_Init()
{
    WiFi.mode(WIFI_STA);
    Serial.println(WIFI_SSID);
    Serial.println(WIFI_PASS);
    WiFi.begin(WIFI_SSID, WIFI_PASS);
    int timeConnect = 0;
    while (WiFi.status() != WL_CONNECTED && timeConnect++ < 20) 
    {
        delay(500);
        Serial.print(".");
    }
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial.println("WiFi not connected");
        return;
    }
    Serial.println("WiFi connected");
    WiFi.config(WiFi.localIP(), WiFi.gatewayIP(), WiFi.subnetMask(), dns);
}

void EEPROM_Write(int address, int value)
{
    EEPROM.write(address, value);
    EEPROM.commit();
}

int EEPROM_Read(int address)
{
    return EEPROM.read(address);
}

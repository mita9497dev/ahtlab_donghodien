# AHTLAB - ESP32 GSM Thiết bị đo điện

**Phần cứng ESP32 GSM được thiết kế bởi [AHTLAB.COM](AHTLAB.COM)**

## Thư viện

- Thư viện [**ESP32 GSM của AHTLAB**](https://gitlab.com/mita9497dev/ahtlab_gsm)
- PZEM400T: Nằm trong thư mục /libs
- Thư viện [**OLED**](https://github.com/ThingPulse/esp8266-oled-ssd1306)

## Lắp mạch

- **Kết nối với PZEM400T**:

 ```c++
 // Chân RX 12
 // Chân TX 13
```

## Cấu hình

- **ID và Secret Key**
  - Trên web: Vào **Thiết bị** > **Cài đặt**
  - Copy **ID** thiết bị và **SECRET KEY** vào code arduino

    ```c++
      #define DEVICE_ID       "6" // ID
      #define DEVICE_SECRET   "123456" // SECRET KEY
    ```

- **RELAY và BUZZER**
  - Cấu hình chân:

  ```c++
    // RELAY
    #define RELAY_PIN       5 // Chân RELAY
    #define RELAY_ON        1
    #define RELAY_OFF       0

    // BUZZER
    #define BUZZER_PIN       5 // Chân BUZZER
    #define BUZZER_ON        1
    #define BUZZER_OFF       0
  ```

## Code hardware

- Vấn đề số điện dư:
  - Khi khởi động thiết bị sử dụng số dư KWH đã lưu trước đó
  - Khi request lên server số dư sẽ được cập nhật lại
  - Số dư dưới 10KWH sẽ báo động qua buzzer

- Hoạt động đọc cảm biến và gửi dữ liệu:
  - Thiết bị đọc giá trị từ cảm biến mỗi 2 giây, sau khi đọc cập nhật lại số dư
  - Gửi dữ liệu lên server mỗi 60 giây
    - Có thể chỉnh lại trong code:

      ```c++
        // TIMEOUT
        #define SEND_TIMEOUT    60000L
      ```
  
- Tự động gửi bằng WiFi nếu có WiFi, nếu không gửi bằng Module GSM

## API

- Viết bằng PHP
- Dữ liệu trả về:
  - "1:BALANCE" -> Thành công với BALANCE là số dư KWH
  - "0:authorization" -> Sai **DEVICE_ID** hoặc **DEVICE_SECRET**
  - "0:params_invalid" -> Giá trị cảm biến chưa hợp lý
  - "0:reset_month" -> Reset dữ liệu tháng của thiết bị chưa thành công (ít xảy ra)
  - "0:cost" -> **Hết tiền**
